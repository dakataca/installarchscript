#!/bin/bash

# Keyboard configuration
echo Configurando Teclado
String="Section \"InputClass\"
		Identifier \"system-keyboard\"
		MatchIsKeyboard \"on\"
                Option \"XkbLayout\"  \"latam\"
                #Option \"XkbModel\"   \"presario\"
                Option \"XkbVariant\" \"deadtilde\"
                Option \"XkbOptions\" \"grp:alt_shift_toggle\"
    EndSection"

echo "$String"> /etc/X11/xorg.conf.d/10-keyboard.conf
