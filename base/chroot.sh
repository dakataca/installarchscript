#!/bin/bash
rm /etc/localtime
ln -sf /usr/share/zoneinfo/America/Bogota /etc/localtime
hwclock -w -u
nano /etc/locale.gen
locale-gen
echo $LANG
export LANG=es_CO.UTF-8
echo LANG=es_CO.UTF-8> /etc/locale.conf
echo KEYMAP=la-latin1> /etc/vconsole.conf
read -p "Digite nombre del equipo" ARCH
echo $ARCH> /etc/hostname
sudo passwd
read -p "Digite nombre de usuario" USERARCH
useradd -m -g users -G audio,lp,optical,storage,video,wheel,games,power,scanner -s /bin/bash $USERARCH
sudo passwd $USERARCH
nano /etc/sudoers
pacman -Syu networkmanager grub os-prober --needed
grub-install /dev/sda
grub-mkconfig -o /boot/grub/grub.cfg 
mkinitcpio -p linux
systemctl enable NetworkManager
exit