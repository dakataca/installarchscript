#!/bin/bash

loadkeys la-latin1
setfont Lat2-Terminus16
#setfont ter-v32n
timedatectl set-ntp true
timedatectl set-timezone America/Bogota
timedatectl status
nano /etc/locale.gen
locale-gen
export LANG=es_CO.UTF-8
echo $LANG
timedatectl status
cfdisk /dev/sda
lsblk
mkfs.ext4 -F /dev/sda3
mkfs.ext4 -F /dev/sda6
mkfs.ext4 -F /dev/sda7
mkswap /dev/sda5
swapon /dev/sda5
mount /dev/sda6 /mnt
mkdir /mnt/boot
mkdir /mnt/home
mount /dev/sda3 /mnt/boot
mount /dev/sda7 /mnt/home
lsblk
nano /etc/pacman.d/mirrorlist
pacstrap /mnt base base-devel 
genfstab -U /mnt >> /mnt/etc/fstab
arch-chroot /mnt /bin/bash